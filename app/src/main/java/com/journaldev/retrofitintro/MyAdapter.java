package com.journaldev.retrofitintro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.journaldev.retrofitintro.pojo.Datum;
import com.journaldev.retrofitintro.pojo.MultipleResource;

import java.security.AccessControlContext;
import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.Viewholder> {

    List<Datum> resource;
    Context context;

    public MyAdapter(List<Datum> resource, Context context) {
        this.resource = resource;
        this.context = context;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_layout,parent,false);

        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        Datum datum = resource.get(position);

        holder.name.setText(datum.getName());
        holder.year.setText(""+datum.getYear());
        holder.color.setText(""+datum.getId());
        holder.pantone_value.setText(datum.getPantoneValue());
    }

    @Override
    public int getItemCount() {
        return resource.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private TextView name,year,color,pantone_value;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            year = itemView.findViewById(R.id.year);
            color = itemView.findViewById(R.id.color);
            pantone_value = itemView.findViewById(R.id.pantone_value);
        }
    }
}
