package com.journaldev.retrofitintro.pojo;

import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    public Integer id;
    @SerializedName("name")
    public String name;
    @SerializedName("year")
    public Integer year;
    @SerializedName("pantone_value")
    public String pantoneValue;

    public Datum(Integer id, String name, Integer year, String pantoneValue) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.pantoneValue = pantoneValue;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getYear() {
        return year;
    }

    public String getPantoneValue() {
        return pantoneValue;
    }
}